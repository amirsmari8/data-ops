it's all about visualization of big data on aws. 
# Amazon QuickSight (serverless): 
- it's for business analytics and visualizations in the cloud. 
- allow all employees in an organization to: 
    * Build visualization. 
    * Perform ad-hoc analysis.
    * quickly get business insight from data.
- QuickSight Data Source: 
* Redshift 
* Aurora/ RDS 
* ATHENA 
* EC2-hosted databases 
- files (S3 or on-premises): execl, csv, ... 
# QuickSight Q: 
- machine learning-powered. 
- answers business questions with natural language processing (what are top-selling items in florida ? )