# Kinesis Analytics
Amazon Kinesis Data Analytics is an AWS service that allows you to process and analyze real-time streaming data with SQL or Apache Flink. It simplifies the task of ingesting, processing, and gaining insights from streaming data, making it easier to build real-time applications, perform analytics, and generate actionable insights from data streams. 

############################################################################################################################################################################
Requirement 3: TRansaction rate alarm. (4,)
############################################################################################################################################################################

# Amazon Opensearch Service (ElasticSearch)
- Elasticsearch start as "search engine" but now it's for "analysis and reporting". 
- for some application and use case , it's faster than pache spark.  
- A search Engine. 
- an analysis tool 
- a visualization tool (dashboard kibana) 
- opensearch can be a "data pipeline": Kinesis replaces Logstash & Beats.  

# Amazon Athena: 
- Serverless interactive queries of S3 data. 
- Interactive query service for S3 (SQL), no need to load data, it stays in S3.
- Presto under the hood. 
- Serverless 
- Support many data formats: 
    * CSV 
    * json 
    * ORC (columnar, splittable)
    * Parquet (columnar, splittable) 
- Unstructured, semi-structured, or structured . 


# Data Warehouse (Amazon Redshift): 
    * Data Warehouse : 
let's explain this with an exmaple: wirless business , in tunise called "Verizon" and france "Jio Digital Life". 
for data we have 2 type of data: 

* structured data: any magasin have 2 software system (see tof 1), the "POS system" track the sales transactions and the "Insurance system" (all client bye an insurance basic , medium or high)
* Unstructured data: we have "cutomer satifaction service" where we scan those servy as pdf file and pdf are stored in amazon s3 

> we need this data to respond to some question: 
1) which store is performing best in terms of device and insurance sales total ? 
2) In term of customer satisfaction which store and employee ranks the best ? 
3) Holiday season is coming, which region is going to have maximum traffic of customers ? 

Response: 
1) which store is performing best in terms of device and insurance sales total ? 
simply we get get the aggrgate sales numbers from "Insurance system" and the aggrgate sales number from the "Insurance system" , But this is not a good idea , because those systm serve real customers and maybe some query will slow the databases. also we need to dome some analytics (tof2) 
once we have all the informations needed about store perfermance, we store those data in a "data warehouse". (tof3) we did exactly ETL : Extract, Transform and Load. 
#
2) In term of customer satisfaction which store and employee ranks the best ? 
3) Holiday season is coming, which region is going to have maximum traffic of customers ? 
after storing all of those informations , data analysis and data science can respond to all others questions (tof4) 

    * OLTP (Online Transaction Processing System) vs OLAP (Online Analytics Processing System) : (tof5)
OLTP: it's just the transaction database which serving the real customers. 
OLAP: needed to perform analytics.


*** Note *** 
Another definition of Data warehouse: is a type of "data management system" that is designed to enable and support "Business intelligence" activities especially "analytics".  

# Amazon Redshift 
Amazon Redshift is an entreprise-level, petabyte scale, fully managed data warehousing service. 
the most importnant "features" of amazon Redshift is "Redshift Spectrum".
Redshift Spectrum: Query exabytes of unstructured data in s3 "without loading". 
