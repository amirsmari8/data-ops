# Collection: (moving data to aws) 
there is 3 main catégorie to move data to aws: 
1 Real Time: Immediate actions: 
    * Kinesis Data Streams (KDS) 
    * Simple queue Service (SQS) 
    * Internet of things (IoT) 
2 Near-real time: reactive actions
    * Kinesis Data firehose (KDF)
    * Database Migration Service (DMS)
3 Batch: for bigdata 
    * Snowball 
    * Data Pipeline 

# Kinesis Data Streams (KDS) 
Kinesis Data Streams is a way to stream big data in our system. KDS is made of multiple "shard", the data will be split across the shard. 
- producers: send data into KDS 'sous forme' of Record. 
- Consumers: consume data in the KDS 
producers can be (AWS SDK, kinesis producer library (KPL), kinesis agent or 3party libraries like spark, Log4j, kafka connect, nifi ...), consumers can be (aws lambda, kinesis data firehose, kinesis data analytics or your own kinesis client library KCL)
## 1 Kinesis Producers: 
how we can produce data into Kinesis data stream ? using sdk,kpl ... 
* SDK or (Kinesis Producer SDK): APIs that are used are "PutRecord" or "PutRecords". 
PutRecord uses batching and increase throughput => less HTTP requests (send many throuput as part of one http request), if we go over the limits we will get "ProvisionedThroughputExceeded" => happen when sending more data (exceeding Mb/s or TPS for any shard)
Example of producers that use SDK in background: cloudwatch logs, aws iot, kinesis data analytics. 
* KPL (kinesis producer library): 
Easy and highly configurable C++ and java library. 
"Batching": both turned on: see tof 
    * Collect: collect and write to multiple shards in the same PutRecords API call 
    * Aggregate: increased latency 
KPL Record must be de-coded with KCL or special helper library. 
* Kinesis Agent: installed on linux environment , it's java based agent, built on top of KPL. 
Kinesis Agent can do Pre-process data before sending to KDS (single line, csv to json, log to json...)
## 2 Kinesis Consumers: 
we use SDK to push data to KDS , also SDK can be consumers. 
others consumers: KCL, kinesis Collector Library also 3party library like spark, Log4j, flume, aws lambda, Kinesis firehose ... 
* SDK: "GetRecords": 
each shard has 2 MB tatal aggregate throughput (consumers) and 1MB as producers. 
GetRecords returns up to 10MB of data or up to 10000 records. 
* KCL (kinesis client Library):  java library but exist for others languages too like nodejs, .NET ... 
Read records from kinesis produced with the KPL (de-aggregation) 
share multiple shards with multiple consumers in one "group" => shard discovery.  
Leverage DynamoDB for coordination and checkpoint (one row per shard) see tof 
* lambda 
aws lambda  can source records from kinesis also can be a consumer , it has a library to de-aggregate record from the KPL. 
lambda can be used to run lightweight ETL to: 
         Amazon S3 -
        - DynamoDB 
        - Redshift 
        - Elasticsearch  
Note: Kinesis Enhanced fan Out 
in KDS we have a limit of 2MB/s per shard, but with "Kinesis Enhanced fan Out" each consumers get 2 MB/s of provisioned throughput per shard. 
## 3 Kinesis Scaling: not important
adding shards = shard splitting 
decrease shard = merging shards

---

# Kinesis Data Firehose   
Kinesis Data Firehose is to store data into target destinantion .
destinantion: S3, Amazon Redshift, Amazon Elasticsearch, or 3party destinantion like datadog, splunk, newRelic ... 
- fully managed service, no administration 
- "Near Real Time" (60 seconds latency) 
- Data Transformation through AWS lambda (ex CSV > JSON) 
# how KDF work ? 
firehose accumulate records in a "buffer". 
the buffer is flushed based on time and size rules  
# Kinesis Data Streams vs Kinesis Data Firehose 
* Streams: 
    going to write custom code (producer/consumer) 
    Real time 
    must manage scaling (shard splitting/merging) 
    Use with lambda to insert data in real-time to elasticsearch 
* Firehose: 
    fully managed, send to s3, Splunk, Redshift, ElasticSearch. 
    Serverless data transformation with lambda 
    Near real time  
# The integration between cloudwatch Logs and kinesis 
You can stream cloudwatch Logs into: 
    * Kinesis Data Streams 
    * kinesis Data Firehose
    * AWS lambda 
By using "Cloudwatch Logs Subscriptions Filters", and we can enable it By using AWS CLI. 
arch 1 : CLoudwatch LOgs Subscription Filter Patterns "Near Real Time" load into amazon ES .
arch 2: Cloudwatch LOgs Subscription Filter Patterns "Real Time" load into amazon ES. 

#### Amazon SQS #### 
- SQS stands for Simple Queue Service.
- With the help of SQS, you can send, store and receive messages between software components at any volume without losing messages.
- Using Amazon sqs, you can separate the components of an application so that they can run independently, easing message management between components. 
- SQS is pull-based, not push-based.
- Messages are 256 KB in size.
- Messages are kept in a queue from 1 minute to 14 days.
- The default retention period is 4 days.
- It guarantees that your messages will be processed at least once.
=> There are two types of Queue:

    * Standard Queues (default): allow duplication in the queue.
    * FIFO Queues (First-In-First-Out): the order in which they are sent is also received in the same order,  does not allow duplicates to be introduced into the Queue.

# Amazon SQS vs Amazon Kinesis data Stream 
*** Kinesis Data Stream: 
- Data can be consumed many time . 
- data is deleted after the retention period. 
- Ordering of records is preserved (at the shard level) even during replays. 
*** SQS: 
- Queue, decouple applications. 
- One application per queue. 
- Records are deleted after consumption 
- Ordering for FIFO queue
# MSK: Managed Streaming Kafka 
- MSK: Fully managed Kafka on AWS
=> Alternative to kinesis , we can build "producers" and "consumers" of data . 

####### MSK vs KDS ######## 
* KDS: 
- hard limit of 1 MB message size limit. 
- Data Stream with Shards 
- Shard splitting and Merging 
* MSK: 
- 1MB default, configure for higher ex 10MB. 
- Kafka Topics with partitions .
- can only add partition to a topic. 
###### REQUIREMENT ########### 
Server Logs >>> Amazon Kinesis Data Firehose (direct put) >>> Amazon s3.
we are using "Kinesis Agent" installed on ec2 (Kinesis Agent can do Pre-process data before sending to KDF (single line, csv to json, log to json...))
* to install kinesis agent: 
$ sudo yum install aws-kinesis-agent 
config file: /etc/aws-kinesis/agent.json
* fake logdata: wget http://media.sundog-soft.com/AWSBigData/LogGenerator.zip: contain python script that generate fake log(generate some csv file under /var/log/cadabra). 
* configuration file : /etc/aws-kinesis/agent.json 
####
{
  "cloudwatch.emitMetrics": true,
  "kinesis.endpoint": "",
  "firehose.endpoint": "firehose.eu-west-3.amazonaws.com",
  "awsAccessKeyId": "",
  "awsSecretAccessKey": "",
  "flows": [
    {
      "filePattern": "/var/log/cadabra/*.log",
      "deliveryStream": "CloudfrontLogs"    #delivery stream name
    }
  ]
}
#### 
sudo service aws-kinesis-agent status 
sudo chkconfig aws-kinesis-agent on 

###### REQUIREMENT 2 ########### 
Servers loGS ------> kinesis-agent (cvtojson) ------> KDS ------------> LAMBDA -----------> DynamoDB ------------> ClientAPP . 
instead of using KDF (kinesis data firehose)wich is "Near-real time" we are going to use  KDS (kinesis data Stream) wich is Real Time Data processing. 
Data Stream name: CloudfrontOrders . 


=> we can send LOG to 2 destination (KDF is an option): 
  * KDS 
  * KDF


{
  "cloudwatch.emitMetrics": true,
  "kinesis.endpoint": "",
  "firehose.endpoint": "",
  "awsAccessKeyId": "",
  "awsSecretAccessKey": "",
  "flows": [
    {
      "filePattern": "/var/log/cadabra/*.log",
      "kinesisStream": "CloudfrontOrders",
      "partitionKeyOption": "RANDOM", 
      "dataProcessingOptions": [
        {
            "optionName": "CSVTOJSON", 
            "customFieldNames": ["InvoiceNo", "stockCode", "Description", "Quantity", "InvoiceDate", "UnitPrice", "Customer", "Country"]
        }
      ]
    }
  ]
}