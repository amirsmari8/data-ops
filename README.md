
## Big Data Pipeline: 
big data world consist of 3 stages/phases/pipelines: 
        * data ingestion 
        * data preprocessing or data cleansing 
        * data analytics
# 1 Data ingestion: 
where the data is maintained or availables ? 
not require nowday!, why ? because the data must be in "reliables" storage system (s3, blob storage, ). 
in past the only flexible or reliables storage system is HDFS 
SO THE CLIENT USE TO MAINTAIN THE DATA IN A CLOUD SERVICE STORAGE + also RDBMS Storage , bigquey, redshift . 
### FORMAT OF DATA ####" 
the data can be in any format : 
- structured  ---> table
- semi-structured ----> csv, json, xml 
- unstructured -----> data not organized 

S3, blob, google cloud storage = object storage system 

# 2 Data preprocessing or data cleansing 
the data must be in an organised way: 
 * input ---> structured, semi-structured, unstructured 
 * output ---> structured data
adding some data/deleting some data, dealing with null values, adding some columns
tools: Spark -----> read, process and store in warehouse . 
* Spark is replaced the traditional ETL tools.  
* Spark must be a distributed compute cluster. (run on kubernetes as example)
# 3 Analysis
the data must be sored is "warehouse" for analysis (rdbms is for transactions purpose not for analysis)
warehouse ---> Hive, Snwoflake(aws)
HDFS is storage file system for "Hive", s3, azure and GCP are storage file systems for Snowflake.  
data ingestion (S3) ------> data preprocessing (Spark ETL tools) ------> data analysis (Snwoflake). 
# Big data pipeline: 
big data pipelines must be automated ----> cronjob, Airflow

###############################################################
############## hadoop Ecosystem ############################## 
##############################################################

Hadoop is an open-source distributed computing framework designed to store and process very large datasets across a cluster of commodity hardware. Hadoop is primarily used for handling big data and is based on the MapReduce programming model.

Hadoop consists of two main components:

1. **Hadoop Distributed File System (HDFS)**: HDFS is a distributed file system that stores data across multiple machines in a Hadoop cluster. It divides large files into smaller blocks (typically 128MB or 256MB in size) and replicates these blocks across different machines for fault tolerance. HDFS is designed to handle massive data storage efficiently.

2. **MapReduce**: MapReduce is a programming model and processing engine for distributed data processing in Hadoop. It allows users to write programs that can process and analyze large datasets in parallel across the cluster. The MapReduce process consists of two phases: the "Map" phase, where data is divided into key-value pairs and processed in parallel, and the "Reduce" phase, where the results from the Map phase are aggregated and processed further.

Here's a simple example of how Hadoop can be used:

**Problem**: Counting the frequency of words in a large collection of text documents.

**Hadoop Workflow**:

1. **Data Ingestion**: You start with a large dataset of text documents that you want to analyze.

2. **Data Storage**: You store these documents in HDFS. Hadoop will automatically split them into smaller blocks and replicate them across the cluster.

3. **Map Phase**: You write a MapReduce program that defines two functions: a "Map" function and a "Reduce" function. In the "Map" function, you read each document, split it into words, and emit key-value pairs for each word, where the key is the word itself, and the value is 1 (indicating one occurrence of the word).

4. **Shuffle and Sort**: Hadoop takes care of sorting and shuffling the key-value pairs to group all occurrences of the same word together.

5. **Reduce Phase**: In the "Reduce" function, you receive the sorted key-value pairs for each word. You sum up the values (the count of occurrences) for each word to get the total frequency.

6. **Output**: The final output is a list of words and their corresponding frequencies.

Hadoop is particularly well-suited for batch processing of large datasets. It has been widely used in various industries, including e-commerce, social media, finance, and scientific research, for tasks such as log analysis, recommendation systems, and scientific simulations.

# Hadoop Ecosystem: Hadoop has a rich ecosystem of tools and technologies like: Hive (warehouse), 
###############################################################################################################################################
###############################################################################################################################################
# Hadoop vs ETL (like spark) 
Hadoop and ETL frameworks like Spark have different core purposes and strengths. Hadoop is primarily designed for distributed batch processing and storage, while ETL frameworks are focused on data integration, transformation, and can support a wider range of processing paradigms, including batch and real-time processing. Often, organizations use both Hadoop and ETL tools together to build end-to-end data processing pipelines that cover various data processing and analytics needs.
###############################################################################################################################################
###############################################################################################################################################

# EMR (ELASTIC MAP  REDUCE)
- it's managed Hadoop framework on EC2 instances. 
- Amazon EMR includes: Spark, Hbase, Presto, Flink, Hive ... 
- EMR cluster contain 3 part: 
        * Master node: manage the cluster
        * Core node: host HDFS (Hadoop distributed file systems) data and run Tasks. 
        * Task node (optional): just run tasks.