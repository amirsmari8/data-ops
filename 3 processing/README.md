# AWS LAMBDA 
* a way to run code snippets in the cloud (serverless + continuous scaling) 
* often used to process "data" as it's moved around (from aws service to another) 
* main use of lambda: 
- Real Time file processing . 
- Real Time stream processing 
- ETL 
- Cron replacement 
- process AWS events 
# Lambda Cost Models 
"Pay for what you use" . 
$0.20/million requests 
$0.00001557 per GB/second 
=> really cheap .  
# aws lambda integration 
* lambda and amazon elasticsearch service . 
s3 ------> aws lambda -----------> aws elasticsearch . 
s3 recieve data from any source, with lambda we can configure a trigger and transform the data or process it and send the data to ELASTICSEARCH to be indexed. 

* lambda and Data Pipeline:
s3 ------> aws lambda -----------> aws dataPipeline.
Data Pipeline can be scheduled with preconditions to check s3, but Lambda lets as activate it at random times. 

* lambda and Redshift:
s3 ------> aws lambda -----------> aws Redshift.
             |
             |
             |
        aws dynameDB   
Best practice for loading data into Redshift is the COPY command, butt you can use Lambda if you need to respond to new data that shows up at any time. 
we can use Dynamo DB to keep track of what's been loaded (why ? because lambda is a stateless service it can't keep track the data )

* lambda and Kinesis: 
your lambda code recieves an event with a batch of streams records (too large a batch size can cause timeouts! )
Lambda will retry the batch until it succeeds or the data expires   

################################################################################################################################################################################################### 

###################################################################################################################################################################################################

# AWS Glue (ETL Tool: Extract, Transform, Load)
aws glue is the fully managed, serverless ETL service . 
* Extract: is the process of fetching/retrieving data from various data sources and load it into a staging area for further use. 
* Transform: is the process of performing operations like cleansing, aggregations, mapping --> the object is to prepare and optimize the data which can be easily consumed by the end process like : Data analytics, ML , ... 
* Load: is the process of iserting the transformed data into the target database, data warehouse ...  

- AWS Glue is used to discover the data, transform it and prepare it for analytics. 
- it consist of Central Metadata Repository known as AWS Glue Data Catalog. 
- Apache spark ETL engine (use saprk as it's engine)
- Flexible scheduler.  
- limited for python and scala developers.

# Componenets of AWS Glue 
* AWS Glue Console: Orchestrate ETL workflows 
* AWS Glue Data catalog: persist metadata store 
* AWS Glue crawlers and classifiers: Detect and infer schemas to store it in Data catalog. (crawlers and classifiers help to determinate the schema of the data. )
* AWS Glue operation: automatics ETL code generations in python or scala. 
* AWS Glue Jobs System: Enable to orchestrate the ETL workflow which can be scheduled or triggered based on events. 

# TP: https://www.youtube.com/watch?v=nMtvGkSSWRo&list=PL5KTLzN85O4KdNBfGpD-QIabS3yvwI4qn&index=2 
we will create a "crawlers" which will connect to the "s3" data store (csv file), the "crawlers" will determine the structure of the csv using "built-in classifier" and it create the metadata tables in "aws glue data catalog". then we will create the "etl job" to transform the "csv" to "parquet" files.
the data source of the "etl job" will be the "aws glue data catalog" table. 
after the transformations, the data will be stored is s3 as "parquet" files. (see tof)  

# EMR (Elastic MapReduce) 
- it's managed "Hadoop" framework on EC2 instances.  
- Hadoop Ecosystem: Hadoop has a rich ecosystem of tools and technologies like: Spark,Hive, Pig ...
* 1 spark: 
- we can run "spark" on top of Amazon EMR (hadoop). 
- SPARK: Distributed Processing framework for big data. 
- Map && Reduce functionality was great 10 years ago, but now we use Spark (because of in-memory caching , it does all of work in memory) (see tof spark)
* 2 Hive: 
- Hive is a way to execute SQL code on our unstructured data that might live in s3, HDFS ... 
 * 3 Pig:  
- a script language let us define our "map" and reduce "steps".  
 * 4 Hbase:  
- hbase is non-relational databases like dynamodb 
 * 5 Zeppelin:  
- just a "notebook" to run python code/script against the data. 

############################################################################################################################################################################
############################################################################################################################################################################
Requirement 2: Product Recommendations. 
############################################################################################################################################################################
############################################################################################################################################################################ 
Server logs ------------------> Amazon Kinesis Firehose ---------------> S3 ---------------> Amazon EMR 

note: we need to spin up and EMR cluster and run "apache Spark" using "ML lib" (machine learning) to generate a "recommendations model" based on that order data in s3. 