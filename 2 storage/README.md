# s3 Storage Class
* Amazon s3 Standard - general purpose // use case: big data, mobile and gaming 
* Amazon s3 Standard - Infrequent Access(IA) // use case: backups and disaster recovery
* Amazon s3 One Zone-Infrequent Access
* Amazon s3 Glacier Instant Retrieval // low cost meant for archiving / backup 
* Amazon s3 Glacier Fle xible Retrieval 
* Amazon s3 Glacier Deep Archive
* Amazon s3 Intelligent Tiering 
# s3 Perfermance: 
* Multi-part upload : 
    - recommended for files > 100MB, must used for file > 5GB 
    - can help paralleze uploads (speed up transfers) 
* s3 Transfers acceleration 
    - Increase transfer speed by transferring file to an "AWS edge location" which will forward the data to s3 bucket in the target region    
# S3 encryption and security: 
there are 4 method of encryption objects in s3: 
    * SSE-S3: encrypts S3 objects using keys handled & managed by AWS 
    * SSE-KMS: leverage AWS key management service to manage encryption keys. 
    * SSE-C: when you want to manage your own encryption keys. 
    * client side Encryption .


####################  Dynamo DB  ####################     
dynamo dB: NO SQL serverless Database .
- dynamo db is made of tables 
- each table has a Primary Key, must be decided at creation time (option 1: partition key - partition key + Sort key)
- each tab can have a,n infinite number of items 
- Data Types supported are: 
    * Scalar Types: string, number, binary, boolean, null. 
    * Document types: List, Map. 
    * Set Types: String Set Number set , binary set .
    
###### REQUIREMENT ########### 
video 36 